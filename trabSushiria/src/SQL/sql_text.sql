/* Create Tables */

CREATE TABLE OO_CLIENTE
(
	id_Cliente number NOT NULL,
	usuario varchar2(20) NOT NULL UNIQUE,
	senha varchar2(30),
	nome varchar2(50),
	PRIMARY KEY (id_Cliente)
);


CREATE TABLE itens_venda
(
	id_sushi number NOT NULL,
	id_Venda number NOT NULL,
	valor float,
	quantidade number
);


CREATE TABLE OO_SUSHI
(
	id_sushi number NOT NULL,
	id_tipo number NOT NULL,
	valor float,
	estoque number,
        sabor varchar2(30),
	PRIMARY KEY (id_sushi)
);


CREATE TABLE OO_TIPO
(
	id_tipo number NOT NULL,
	nome varchar2(23),
	PRIMARY KEY (id_tipo)
);


CREATE TABLE OO_VENDA
(
	id_Venda number NOT NULL,
	id_Cliente number NOT NULL,
	data_Venda date,
	pagamento varchar2(30),
	vendedor varchar2(30),
	PRIMARY KEY (id_Venda)
);

/* Create Foreign Keys */

ALTER TABLE OO_VENDA
	ADD FOREIGN KEY (id_Cliente)
	REFERENCES OO_CLIENTE (id_Cliente)
;


ALTER TABLE itens_venda
	ADD FOREIGN KEY (id_sushi)
	REFERENCES OO_SUSHI (id_sushi)
;


ALTER TABLE OO_SUSHI
	ADD FOREIGN KEY (id_tipo)
	REFERENCES OO_TIPO (id_tipo)
;


ALTER TABLE itens_venda
	ADD FOREIGN KEY (id_Venda)
	REFERENCES OO_VENDA (id_Venda)
;



