package classes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Sushi implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private Integer id_Sushi;

    private Tipo tipo;

    private Float valor;

    private int estoque;

    private String sabor;

    private Set<ItensVenda> itensVendaSet;

    /**
     * Constructor.
     */
    public Sushi() {
        this.itensVendaSet = new HashSet<ItensVenda>();
    }

    public void setId_Sushi(Integer id_Sushi) {
        this.id_Sushi = id_Sushi;
    }

    public Integer getId_Sushi() {
        return this.id_Sushi;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Tipo getTipo() {
        return this.tipo;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Float getValor() {
        return this.valor;
    }

    public void setItensVendaSet(Set<ItensVenda> itensVendaSet) {
        this.itensVendaSet = itensVendaSet;
    }

    public void addItensVenda(ItensVenda itensVenda) {
        this.itensVendaSet.add(itensVenda);
    }

    public Set<ItensVenda> getItensVendaSet() {
        return this.itensVendaSet;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id_Sushi == null) ? 0 : id_Sushi.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Sushi other = (Sushi) obj;
        if (id_Sushi == null) {
            if (other.id_Sushi != null) {
                return false;
            }
        } else if (!id_Sushi.equals(other.id_Sushi)) {
            return false;
        }
        return true;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public int inserirSushi(Sushi sushi) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int id_Sushi = 0;

        String insertTableSQL = "INSERT INTO OO_Sushi (id_Sushi,id_tipo,valor,estoque,sabor) VALUES (IDSUSHI_SEQ.nextVal,?,?,?,?)";

        try {

            String generatedColumns[] = {"id_Sushi"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();

            ps.setInt(1, tipo.getId_Tipo());
            ps.setFloat(2, valor);
            ps.setInt(3, estoque);
            ps.setString(4, sabor);
            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Sushi table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            sushi.setId_Sushi(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_Sushi;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    static public ArrayList<Sushi> getAll() { // Retorna um array contendo todos os sushis no banco de dados
        Conexao c = new Conexao();
        ArrayList<Sushi> al = new ArrayList<Sushi>();
        Connection con = c.getConexao();
        PreparedStatement ps = null, ps2 = null;
        String sql = "select * from OO_Sushi";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r = ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                Sushi l = new Sushi(); // se existe uma resposta, cria um novo Cliente
                int id_tipo = r.getInt("id_Tipo");

                String sqlTipo = "select * from oo_tipo where id_tipo = " + id_tipo;

                ps2 = con.prepareStatement(sqlTipo);
                ResultSet r2 = ps2.executeQuery(); // e
                
                if(r2.next()){
                    Tipo tTemp = new Tipo(r2.getString("nome"));
                    tTemp.setId_Tipo(id_tipo);
                    l.setTipo(tTemp);
                }
                
                
                l.setId_Sushi(r.getInt("id_Sushi"));
                l.setValor(r.getFloat("valor"));
                l.setEstoque(r.getInt("estoque"));
                l.setSabor(r.getString("sabor"));

                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }

    public void delete() { // deleta pelo nome
        Conexao c = new Conexao();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String insertTableSQL = "DELETE OO_Sushi where sabor=?";
        try {
            ps = con.prepareStatement(insertTableSQL);
            ps.setString(1, sabor);
            ps.executeUpdate();
            System.out.println("Foi excluido da tabela Sushi!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update() { // muda uma linha
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_Sushi SET id_tipo=?, valor=?, estoque=? where sabor=?";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, tipo.getId_Tipo());
            preparedStatement.setFloat(2, valor);
            preparedStatement.setInt(3, estoque);
            preparedStatement.setString(4, sabor);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado na tabela Sushi!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return  sabor ;
    }

}
