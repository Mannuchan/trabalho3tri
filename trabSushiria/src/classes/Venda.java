package classes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Venda implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private Integer id_Venda;

    private Date dataVenda;

    private String pagamento;

    private String vendedor;

    private Cliente id_Cliente;

    private Tipo tipo;

    private Sushi sushi;

    private Set<ItensVenda> itensVendaSet;

    private ItensVenda quantidade;

    /**
     * Constructor.
     */
    public Venda() {
        this.itensVendaSet = new HashSet<ItensVenda>();
    }

    public void setId_Venda(Integer id_Venda) {
        this.id_Venda = id_Venda;
    }

    public Integer getId_Venda() {
        return this.id_Venda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public Date getDataVenda() {
        return this.dataVenda;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public String getPagamento() {
        return this.pagamento;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getVendedor() {
        return this.vendedor;
    }

    public void setId_Cliente(Cliente id_Cliente) {
        this.id_Cliente = id_Cliente;
    }

    public Cliente getId_Cliente() {
        return this.id_Cliente;
    }

    public void setItensVendaSet(Set<ItensVenda> itensVendaSet) {
        this.itensVendaSet = itensVendaSet;
    }

    public void addItensVenda(ItensVenda itensVenda) {
        this.itensVendaSet.add(itensVenda);
    }

    public Set<ItensVenda> getItensVendaSet() {
        return this.itensVendaSet;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id_Venda == null) ? 0 : id_Venda.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Venda other = (Venda) obj;
        if (id_Venda == null) {
            if (other.id_Venda != null) {
                return false;
            }
        } else if (!id_Venda.equals(other.id_Venda)) {
            return false;
        }
        return true;
    }

    public int inserirVenda(Venda venda) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int id_Sushi = 0;

        String insertTableSQL = "INSERT INTO OO_Venda (id_Venda,datavenda,pagamento,vendedor,id_cliente) VALUES (IDVENDA_SEQ.nextval,?,?,?,?)";

        try {

            String[] generatedColumns = {"id_Venda"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();

            ps.setDate(1, (java.sql.Date) dataVenda);
            ps.setString(2, pagamento);
            ps.setString(3, vendedor);
            ps.setInt(4, id_Cliente.getId_Cliente());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Venda table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            venda.setId_Venda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_Venda;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Sushi getSushi() {
        return sushi;
    }

    public void setSushi(Sushi sushi) {
        this.sushi = sushi;
    }

    public ItensVenda getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(ItensVenda quantidade) {
        this.quantidade = quantidade;
    }

    public static ArrayList<Venda> getAll() {
        Conexao c = new Conexao();
        ArrayList<Venda> al = new ArrayList<Venda>();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String sql = "select *from OO_Venda";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r = ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                Venda l = new Venda(); // se existe uma resposta, cria uma nova Venda
                l.setId_Venda(r.getInt("id_Venda"));
                l.setDataVenda(r.getDate("dataVenda"));
                l.setPagamento(r.getString("pagamento"));
                l.setVendedor(r.getString("vendedor"));
                Cliente cli = new Cliente();
                //cli.load(); para pegar tudo dentro da classe cliente  
                cli = cli.getById(r.getInt("id_Cliente"));
                if (cli == null) {
                    cli = new Cliente();
                    cli.setNome("Não Encontrado");
                }
                l.setId_Cliente(cli);
                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }

}
