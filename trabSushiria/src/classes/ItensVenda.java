package classes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ItensVenda implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private Sushi id_Sushi;

    private Venda id_Venda;

    private float valor;

    private String sabor;

    private String produto;

    private float precoTotal;

    private int quantidade;

    /**
     * Constructor.
     */
    public ItensVenda() {
    }

    public void setId_Sushi(Sushi id_Sushi) {
        this.id_Sushi = id_Sushi;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public float getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(Float precoTotal) {
        this.precoTotal = precoTotal;
    }

    public Sushi getId_Sushi() {
        return this.id_Sushi;
    }

    public void setId_Venda(Venda id_Venda) {
        this.id_Venda = id_Venda;
    }

    public Venda getId_Venda() {
        return this.id_Venda;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public float getValor() {
        return this.valor;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getQuantidade() {
        return this.quantidade;
    }

    public void inserirItensVenda(ItensVenda itensVenda) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO itens_venda (id_sushi,id_venda,valor,quantidade) VALUES (?,?,?,?)";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, id_Sushi.getId_Sushi());
            preparedStatement.setInt(2, id_Venda.getId_Venda());
            preparedStatement.setFloat(3,valor);
            preparedStatement.setInt(4, quantidade);
            preparedStatement.executeUpdate();
            System.out.println("Foi inserido na tabela ItensVenda!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<ItensVenda> getAll() {
        Conexao c = new Conexao();
        ArrayList<ItensVenda> al = new ArrayList<ItensVenda>();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String sql = "select *from Itens_Venda";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r = ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                ItensVenda l = new ItensVenda(); // se existe uma resposta, cria uma nova Venda

                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }

}
