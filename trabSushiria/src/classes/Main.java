package classes;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        URL u= getClass().getClassLoader().getResource("telas/Inicio.fxml"); //getClass().getClassLoader().getResource => important to solve bugs
        Parent root = FXMLLoader.load(u);
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
