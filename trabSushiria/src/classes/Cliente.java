package classes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Cliente implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private Integer id_Cliente;

	private String nome;

	private String usuario;

	private String senha;

	private Set<Venda> vendaSet;

	/**
	 * Constructor.
	 */
	public Cliente() {
		this.vendaSet = new HashSet<Venda>();
	}

	public void setId_Cliente(Integer id_Cliente) {
		this.id_Cliente = id_Cliente;
	}

	public Integer getId_Cliente() {
		return this.id_Cliente;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setVendaSet(Set<Venda> vendaSet) {
		this.vendaSet = vendaSet;
	}

	public void addVenda(Venda venda) {
		this.vendaSet.add(venda);
	}

	public Set<Venda> getVendaSet() {
		return this.vendaSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_Cliente == null) ? 0 : id_Cliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cliente other = (Cliente) obj;
		if (id_Cliente == null) {
			if (other.id_Cliente != null) {
				return false;
			}
		} else if (!id_Cliente.equals(other.id_Cliente)) {
			return false;
		}
		return true;
	}
        
        public int inserirCliente(Cliente cliente) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int id_Cliente = 0;

        String insertTableSQL = "INSERT INTO OO_Cliente (id_Cliente, nome, usuario, senha) VALUES (IDCLIENTE_SEQ.nextVal,?,?,?)";

        try {

            String generatedColumns[] = {"id_Cliente"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getUsuario());
            ps.setString(3, cliente.getSenha());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Cliente table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            cliente.setId_Cliente(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_Cliente;
    }
                 
    static public ArrayList<Cliente> getAll() { // Retorna um array contendo todos os clientes no banco de dados
        Conexao c = new Conexao();
        ArrayList<Cliente> al = new ArrayList<Cliente>();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String sql = "select *from OO_Cliente";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r=ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                Cliente l = new Cliente(); // se existe uma resposta, cria um novo Cliente
                l.setNome(r.getString("nome"));
                l.setId_Cliente(r.getInt("id_Cliente"));
                l.setUsuario(r.getString("usuario"));
                l.setSenha(r.getString("senha"));
                al.add(l); 
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }
    
    public Cliente getById(int id){
        Cliente c = null;
        
        ArrayList<Cliente> al = getAll();
        
        for(Cliente cTemp : al){
            if(cTemp.getId_Cliente() == id) c = cTemp;
        }
        return c;
    }
    
    
    public void delete() { // deleta pelo nome
        Conexao c = new Conexao();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String insertTableSQL = "DELETE OO_Cliente where nome=?";
        try {
            ps = con.prepareStatement(insertTableSQL);
            ps.setString(1, nome);
            ps.executeUpdate();
            System.out.println("Foi excluido da tabela Cliente!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void update() { // muda uma linha
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_Cliente SET usuario=?, senha=? where nome=?";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, usuario);
            preparedStatement.setString(2, senha);
            preparedStatement.setString(3, nome);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado na tabela Cliente!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return nome ;
    }
}
