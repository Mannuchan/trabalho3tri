package classes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Tipo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private Integer id_Tipo;
        
	private String nome;

	private Set<Sushi> sushiSet;

	/**
	 * Constructor.
	 */
	public Tipo() {
		this.sushiSet = new HashSet<Sushi>();
	}

    public Tipo(String nome) {
       this.nome = nome;
    }

	public void setId_Tipo(Integer id_Tipo) {
		this.id_Tipo = id_Tipo;
	}

	public Integer getId_Tipo() {
		return this.id_Tipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setSushiSet(Set<Sushi> sushiSet) {
		this.sushiSet = sushiSet;
	}

	public void addSushi(Sushi sushi) {
		this.sushiSet.add(sushi);
	}

	public Set<Sushi> getSushiSet() {
		return this.sushiSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_Tipo == null) ? 0 : id_Tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tipo other = (Tipo) obj;
		if (id_Tipo == null) {
			if (other.id_Tipo != null) {
				return false;
			}
		} else if (!id_Tipo.equals(other.id_Tipo)) {
			return false;
		}
		return true;
	}
              
    static public ArrayList<Tipo> getAll() { // Retorna um array contendo todos os tipos no banco de dados
        Conexao c = new Conexao();
        ArrayList<Tipo> al = new ArrayList<Tipo>();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String sql = "select *from OO_Tipo";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r=ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                Tipo l = new Tipo(); // se existe uma resposta, cria um novo Tipo
                l.setNome(r.getString("nome"));
                l.setId_Tipo(r.getInt("id_tipo"));
                al.add(l); 
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }

    @Override
    public String toString() {
        return nome ;
    }
    
    

}
