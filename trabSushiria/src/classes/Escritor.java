package classes;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

public class Escritor {

    public static void createLog(String fileName, String content) {
        File file = new File(fileName);
        try {

            FileWriter fr = new FileWriter(file, true);
            fr.write("[" + new Date() + "] - " +content + "\r\n");
            fr.close();
            
        } catch (Exception e) {

        }
    }

}
