package telas;

import classes.Cliente;
import classes.Conexao;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class ClienteController implements Initializable {

    @FXML
    private TextField nomeTF;
    @FXML
    private TextField usuarioTF;
    @FXML
    private PasswordField senhaPF,reSenhaPF;
    @FXML
    private Label aviso;
    @FXML
    private TableView<Cliente> tabela;
    @FXML
    private TableColumn<Cliente, String> nomeCol;
    @FXML
    private TableColumn<Cliente, String> usuarioCol;
    @FXML
    private TableColumn<Cliente, String> senhaCol;
    
    private ObservableList<Cliente> cliente;
     
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        cliente = tabela.getItems();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        usuarioCol.setCellValueFactory(new PropertyValueFactory<>("usuario"));
        senhaCol.setCellValueFactory(new PropertyValueFactory<>("senha"));      
        this.tabela.setItems(cliente);
        adicionaClientes();
        
    }    

    @FXML
    private void voltar(ActionEvent event) throws Exception {
         replaceScene("Inicio.fxml", event);
    }

    @FXML
    private void confirmar(ActionEvent event) {
        if(!(nomeTF.getText().isEmpty() || senhaPF.getText().isEmpty() || reSenhaPF.getText().isEmpty()  || usuarioTF.getText().isEmpty())){
            if(!(this.verificaSenhaIgual(senhaPF.getText(), reSenhaPF.getText()))){
         aviso.setText("Digite uma senha igual nos dois campos...");
     }else{
            Cliente l=new Cliente();
            l.setNome(nomeTF.getText());
            l.setSenha(senhaPF.getText());
            l.setUsuario(usuarioTF.getText());
            l.inserirCliente(l);
            
                aviso.setText("Cadastrado com sucesso");
                nomeTF.clear();
                senhaPF.clear();
                reSenhaPF.clear();
                usuarioTF.clear();
                adicionaClientes();
        }    
    }else{
        aviso.setText("Preencha todos os campos...");
        }
    }
    
      private boolean verificaSenhaIgual(String senha1, String senha2){
     if(senha1.equals(senha2)) 
          return true;
     return false;
 }
      
    private void adicionaClientes(){
        cliente.clear();
        ArrayList<Cliente> al=Cliente.getAll();
        for(int x=0;x<al.size();x++)cliente.add(al.get(x));
    }

    public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }

    @FXML
    private void deletar(ActionEvent event) {
        Cliente l=tabela.getSelectionModel().getSelectedItem();
        if(l==null)return;
        Conexao c= new Conexao();
        try {
            c.getConexao().prepareStatement("delete from OO_Cliente where nome='"+l.getNome()+"'").execute();
            aviso.setText("O item selecionado foi apagado");
        } catch (SQLException ex) {ex.printStackTrace();}
        c.desconecta();
        adicionaClientes();

    }

    @FXML
    private void deletarTudo(ActionEvent event) {
        Conexao c= new Conexao();
        try {
            c.getConexao().createStatement().execute("delete from OO_Cliente");
            aviso.setText("Todos os dados foram apagados");
        } catch (SQLException ex) {ex.printStackTrace();}
        c.desconecta();
        adicionaClientes();
    }

    @FXML
    private void atualizar(ActionEvent event) {
        tabela.getSelectionModel().getSelectedItem().delete();
        Cliente l=tabela.getSelectionModel().getSelectedItem();
        usuarioTF.setText(""+l.getUsuario());
        nomeTF.setText(l.getNome());
        senhaPF.setText(""+l.getSenha());
        reSenhaPF.setText(""+l.getSenha());
        l.update();
        cliente.remove(tabela.getSelectionModel().getSelectedItem());
    }
}
