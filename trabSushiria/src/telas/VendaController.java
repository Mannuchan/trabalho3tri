package telas;

import classes.Cliente;
import classes.Conexao;
import classes.Escritor;
import classes.ItensVenda;
import classes.Sushi;
import classes.Tipo;
import classes.Venda;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class VendaController implements Initializable {

    @FXML
    private TextField vendedorTF;
    @FXML
    private DatePicker dataVendaDP;
    @FXML
    private ChoiceBox<String> pagamentoCB;
    @FXML
    private Label aviso;
    @FXML
    private ChoiceBox<Cliente> clienteCB;
    @FXML
    private TableView<ItensVenda> tabela;
    @FXML
    private ChoiceBox<Tipo> produtoCB;
    @FXML
    private TextField quantidadeTF;
    @FXML
    private TableColumn<Tipo, String> produtoCol;
    @FXML
    private TableColumn<ItensVenda, Integer> quantidadeCol;

    private ObservableList<ItensVenda> itensVenda;
    @FXML
    private TableColumn<Sushi, String> saborCol;
    @FXML
    private TableColumn<Sushi, Float> precoCol;
    @FXML
    private TableColumn<ItensVenda, Float> precoTotalCol;
    @FXML
    private ChoiceBox<Sushi> saborCB;
    @FXML
    private Label estoqueLabel;

    private Venda venda = new Venda();
    @FXML
    private Button atualiza;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualiza.setVisible(false);
        pagamentoCB.getItems().add("Dinheiro");
        pagamentoCB.getItems().add("Cartão");
        pagamentoCB.getItems().add("Boleto");

        for (Cliente t : Cliente.getAll()) {
            clienteCB.getItems().add(t);
        }

        itensVenda = tabela.getItems();
        produtoCol.setCellValueFactory(new PropertyValueFactory<>("produto"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        saborCol.setCellValueFactory(new PropertyValueFactory<>("sabor"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        precoTotalCol.setCellValueFactory(new PropertyValueFactory<>("precoTotal"));
        this.tabela.setItems(itensVenda);
        itensVenda.clear();
        ArrayList<Tipo> al = Tipo.getAll();
        for (int x = 0; x < al.size(); x++) {
            produtoCB.getItems().add(al.get(x));
        }

        produtoCB.valueProperty().addListener(new javafx.beans.value.ChangeListener<Tipo>() {
            @Override
            public void changed(ObservableValue<? extends Tipo> observable, Tipo oldValue, Tipo newValue) {
                atualizaSabor(newValue);
            }

        });

        saborCB.valueProperty().addListener(new javafx.beans.value.ChangeListener<Sushi>() {
            @Override
            public void changed(ObservableValue<? extends Sushi> observable, Sushi oldValue, Sushi newValue) {
                if (newValue == null) {
                    return;
                }
                estoqueLabel.setText("" + newValue.getEstoque());
            }

        });
        carregarCarrinho();
    }

    @FXML
    private void voltar(ActionEvent event) throws Exception {
        replaceScene("Inicio.fxml", event);
    }

    @FXML
    private void confirmar(ActionEvent event) {
        if ((vendedorTF.getText().isEmpty() || pagamentoCB.getSelectionModel().isEmpty() || clienteCB.getSelectionModel().isEmpty() || dataVendaDP.getValue() == null)) {
            aviso.setText("Preencha todos os campos...");
        } else {
            aviso.setText("Cadastro");
            preencheVenda();
            venda.inserirVenda(venda);
            aviso.setText("Cadastrado com sucesso");
            String log = " Cliente " + venda.getId_Cliente() + " comprou: ";
            boolean flag = true;
            for (ItensVenda iv : itensVenda) {
                if (flag) {
                    flag = false;
                } else {
                    log += ", ";
                }
                log += iv.getQuantidade() + " " + iv.getId_Sushi().getTipo().getNome() + " de " + iv.getId_Sushi().getSabor();
                iv.setId_Venda(venda);
                iv.inserirItensVenda(iv);

            }
            log += " do(a) vendedor(a) " + venda.getVendedor() + " através de " + venda.getPagamento() + " na data " + venda.getDataVenda();
            Escritor.createLog("logs.txt", log);

            pagamentoCB.getSelectionModel().clearSelection();
            vendedorTF.clear();
            clienteCB.getSelectionModel().clearSelection();
            dataVendaDP.getEditor().clear();
            itensVenda.clear();
            File file = new File ("venda.manu");
            file.delete();
        }
    }

    public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }

    private void carregarCarrinho() {
        try {
            FileInputStream fout = new FileInputStream("venda.manu");
            ObjectInputStream oos = new ObjectInputStream(fout);
            List<ItensVenda> lista = (ArrayList<ItensVenda>) oos.readObject();
            for(ItensVenda iv : lista){
                itensVenda.add(iv);
            }
            oos.close();
            fout.close();
        } catch (EOFException | NotSerializableException | FileNotFoundException ex) {
            salvarCarrinho();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void preencheVenda() {
        venda.setPagamento(pagamentoCB.getSelectionModel().getSelectedItem());
        venda.setVendedor(String.valueOf(vendedorTF.getText()));
        venda.setId_Cliente(clienteCB.getSelectionModel().getSelectedItem());
        venda.setDataVenda(Date.valueOf(dataVendaDP.getValue()));
    }

    private void salvarCarrinho() {
        try {
            FileOutputStream fout = new FileOutputStream("venda.manu");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            System.out.println(itensVenda);
            oos.writeObject(new ArrayList<ItensVenda>(itensVenda));
            oos.close();
            fout.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void adicionaProdutos() {
        ItensVenda l = new ItensVenda();
        Sushi escolhido = saborCB.getSelectionModel().getSelectedItem();
        l.setId_Sushi(escolhido);
        String qtdDigitadaS = quantidadeTF.getText();
        if (qtdDigitadaS.equals("")) {

            aviso.setText("Quantidade digitada incorreta!");
            return;
        }
        int qtdDigitada = Integer.parseInt(qtdDigitadaS);
        if (qtdDigitada <= escolhido.getEstoque()) {
            escolhido.setEstoque(escolhido.getEstoque() - qtdDigitada);
        } else {
            aviso.setText("Quantidade digitada maior que o estoque!");
            return;
        }
        l.setQuantidade(qtdDigitada);
        l.setValor(escolhido.getValor());
        l.setProduto(escolhido.getTipo().getNome());
        l.setSabor(escolhido.getSabor());
        l.setPrecoTotal(escolhido.getValor() * qtdDigitada);
        quantidadeTF.clear();
        produtoCB.getSelectionModel().clearSelection();
        saborCB.getSelectionModel().clearSelection();
        itensVenda.add(l);
        salvarCarrinho();
    }

    private void atualizaSabor(Tipo tipo) {
        saborCB.getItems().clear();
        estoqueLabel.setText("");
        if (!produtoCB.getSelectionModel().isEmpty()) {
            String sql = "SELECT * from oo_sushi where id_tipo = " + tipo.getId_Tipo();
            for (ItensVenda iv : itensVenda) {
                String nome = " and sabor <> '";
                nome += iv.getId_Sushi().getSabor() + "'";
                sql += nome;
            }
            Conexao c = new Conexao();
            Connection con = c.getConexao();
            PreparedStatement ps = null;

            try {
                ps = con.prepareStatement(sql);
                ResultSet r = ps.executeQuery(); // executa a string no statement
                while (r.next()) {
                    Sushi s = new Sushi();
                    s.setId_Sushi(r.getInt("id_sushi"));
                    s.setValor((float) r.getInt("valor"));
                    s.setTipo(tipo);
                    s.setEstoque(r.getInt("estoque"));
                    s.setSabor(r.getString("sabor"));
                    saborCB.getItems().add(s);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            saborCB.getItems().clear();
        }
    }

    @FXML
    private void verVendas(ActionEvent event) throws Exception {
        replaceScene("VendasConcluidas.fxml", event);
    }

    @FXML
    private void atualizar(ActionEvent event) {
        tabela.getSelectionModel().getSelectedItem().setProduto(produtoCB.getSelectionModel().toString());
        tabela.getSelectionModel().getSelectedItem().setSabor(saborCB.getSelectionModel().toString());
        tabela.getSelectionModel().getSelectedItem().setQuantidade(Integer.parseInt(quantidadeTF.getText()));
        adicionaProdutos();
    }

    @FXML
    private void mostrarAtualizar(MouseEvent event) {
        if (tabela.getSelectionModel().getSelectedItem() != null) {
            atualiza.setVisible(true);
        }
    }
}
