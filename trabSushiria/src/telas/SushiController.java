package telas;

import classes.Conexao;
import classes.Sushi;
import classes.Tipo;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class SushiController implements Initializable {
    
    @FXML
    private ChoiceBox<Tipo> tipoCB;
    @FXML
    private TextField valorTF,estoqueTF;
    @FXML
    private Label aviso;
    @FXML
    private TextField saborTF;
    @FXML
    private TableView<Sushi> tabela;
    @FXML
    private TableColumn<Sushi, String> tipoCol;
    @FXML
    private TableColumn<Sushi, Float> valorCol;
    @FXML
    private TableColumn<Sushi, Integer> estoqueCol;
    @FXML
    private TableColumn<Sushi, String> saborCol;
    
    private ObservableList<Sushi> sushi;

    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        for(Tipo t : Tipo.getAll()){
            tipoCB.getItems().add(t);
        }
        
        sushi = tabela.getItems();
        tipoCol.setCellValueFactory(new PropertyValueFactory<>("tipo"));      
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        estoqueCol.setCellValueFactory(new PropertyValueFactory<>("estoque")); 
        saborCol.setCellValueFactory(new PropertyValueFactory<>("sabor"));      
        this.tabela.setItems(sushi);
        adicionaSushis();
    }    

    @FXML
    private void voltar(ActionEvent event) throws Exception {
         replaceScene("Inicio.fxml", event);
    }

    @FXML
    private void confirmar(ActionEvent event) {
        if(( valorTF.getText().isEmpty() || estoqueTF.getText().isEmpty() || saborTF.getText().isEmpty())){
            aviso.setText("Preencha todos os campos...");
        }else{
            Sushi l=new Sushi();
            l.setTipo(tipoCB.getSelectionModel().getSelectedItem());
            l.setValor(Float.valueOf(valorTF.getText()));
            l.setEstoque(Integer.valueOf(estoqueTF.getText()));
            l.setSabor(saborTF.getText());
            l.inserirSushi(l);
            
                aviso.setText("Cadastrado com sucesso");
                tipoCB.getSelectionModel().clearSelection();
                valorTF.clear();
                estoqueTF.clear();
                saborTF.clear();
                adicionaSushis();
        }           
    }
    private void adicionaSushis(){
        sushi.clear();
        ArrayList<Sushi> al=Sushi.getAll();
        for(int x=0;x<al.size();x++)sushi.add(al.get(x));
    }
    
    public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }

    @FXML
    private void deletar(ActionEvent event) {
        Sushi l=tabela.getSelectionModel().getSelectedItem();
        if(l==null)return;
        Conexao c= new Conexao();
        try {
            c.getConexao().prepareStatement("delete from OO_Sushi where sabor='"+l.getSabor()+"'").execute();
            aviso.setText("O item selecionado foi apagado");
        } catch (SQLException ex) {ex.printStackTrace();}
        c.desconecta();
        adicionaSushis();

    }

    @FXML
    private void deletarTudo(ActionEvent event) {
        Conexao c= new Conexao();
        try {
            c.getConexao().createStatement().execute("delete from OO_Sushi");
            aviso.setText("Todos os dados foram apagados");
        } catch (SQLException ex) {ex.printStackTrace();}
        c.desconecta();
        adicionaSushis();
    }

    @FXML
    private void atualizar(ActionEvent event) {
        tabela.getSelectionModel().getSelectedItem().delete();
        Sushi l=tabela.getSelectionModel().getSelectedItem();
        tipoCB.setValue(l.getTipo());
        valorTF.setText(""+l.getValor());
        estoqueTF.setText(""+l.getEstoque());
        saborTF.setText(""+l.getSabor());
        l.update();
        sushi.remove(tabela.getSelectionModel().getSelectedItem());
    }
}
