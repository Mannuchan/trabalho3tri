package telas;

import classes.Cliente;
import classes.Venda;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class VendasConcluidasController implements Initializable {

    @FXML
    private TableView<Venda> tabelaVenda;
    @FXML
    private TableColumn<Venda, Integer> idVendaCol;
    @FXML
    private TableColumn<Venda, Date> dataVendaCol;
    @FXML
    private TableColumn<Venda, String> pagamentoCol;
    @FXML
    private TableColumn<Venda, String> vendedorCol;
    @FXML
    private TableColumn<Venda, String> clienteCol;
    private ObservableList<Venda> venda;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        venda = tabelaVenda.getItems();
        idVendaCol.setCellValueFactory(new PropertyValueFactory<>("id_Venda"));
        dataVendaCol.setCellValueFactory(new PropertyValueFactory<>("dataVenda"));
        pagamentoCol.setCellValueFactory(new PropertyValueFactory<>("pagamento"));
        vendedorCol.setCellValueFactory(new PropertyValueFactory<>("vendedor"));
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("id_Cliente"));
        ArrayList<Venda> al = Venda.getAll();
        for(Venda v : al){
            venda.add(v);
        }      
    }    

    @FXML
    private void voltar(ActionEvent event)  throws Exception {
        replaceScene("Venda.fxml", event);
    }
    
      public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}
