package telas;

import classes.Escritor;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class InicioController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) { }    

    @FXML
    private void cliente(ActionEvent event) throws Exception {        
         Escritor.createLog("logs.txt", "Clicou no Cliente");
         replaceScene("Cliente.fxml", event);
    }

    @FXML
    private void sushi(ActionEvent event) throws Exception {
         Escritor.createLog("logs.txt", "Clicou no Sushi");
         replaceScene("Sushi.fxml", event); 
    }

    @FXML
    private void venda(ActionEvent event) throws Exception {
         Escritor.createLog("logs.txt", "Clicou na Venda");
         replaceScene("Venda.fxml", event);
    }
    
     public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}
